instance_type       = "t2.medium"
region              = "us-east-1"
name                = "server"
bootstrap           = true
bootstrap_jenkins   = true
bootstrap_nexus     = false
bootstrap_sonarqube = false
bootstrap_tomcat    = false